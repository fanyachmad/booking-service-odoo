from odoo import api, fields, models

class ProductTemplate(models.Model):
    _name = 'team.service.line'

    product_id = fields.Many2one('product.product', string='Equipment')
    employee = fields.Many2one('hr.employee', string='Employee')
    serial_number = fields.Many2one('stock.production.lot', string='Serial No')
    team_id = fields.Many2one('team.service', "Team", ondelete='cascade', index=True)
    sale_id = fields.Many2one('sale.order',"Sale_id", ondelete='cascade', index=True)