from odoo import api, fields, models
from odoo.exceptions import UserError

class ProductTemplate(models.Model):
    _name = 'team.service'

    team_name = fields.Char('Reference', default='New')
    team_leader = fields.Many2one('hr.employee', string='Team Leader')
    team_lines = fields.One2many('team.service.line', 'team_id', 'Operations')
    state = fields.Selection([('draft', 'Draft'), ('confirm', 'Confirmed'), ('cancel', 'Cancelled')], string='Status',
                             default='draft')

    @api.multi
    def action_button_draft(self):
        if self.filtered(lambda inv: inv.state not in ('cancel')):
            raise UserError(_('Plan must be on cancel state '))
        return self.write({'state': 'draft'})

    @api.multi
    def action_button_confirm(self):
        if self.filtered(lambda inv: inv.state not in ('draft')):
            raise UserError(_('Plan must be on draft state '))
        return self.write({'state': 'confirm'})

    @api.multi
    def action_button_cancel(self):
        if self.filtered(lambda inv: inv.state not in ('confirm')):
            raise UserError(_('Plan must be on confirmed state '))
        return self.write({'state': 'cancel'})