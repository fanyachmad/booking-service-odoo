from odoo import api, fields, models

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    is_an_equipment = fields.Boolean('Is An Equipment')