from odoo import api, fields, models
from odoo.exceptions import UserError

class ProductTemplate(models.Model):
    _inherit = 'sale.order'

    is_a_booking = fields.Boolean('Is A Booking')
    team_id = fields.Many2one('team.service', string='Team')
    leader = fields.Many2one('hr.employee', string='Team Leader')
    booking_start = fields.Date('Booking Start')
    booking_end = fields.Date('Booking End')
    team_lines = fields.One2many('team.service.line', 'team_id', 'Operations')