{
    'name': 'Booking Service',
    'version': '1.0',
    'category': 'Sales',
    'author' : 'Fany Achmad',
    'depends': ['product','stock','sale'],
    'data' : [
        'views/team_views.xml',
        'views/sale_views.xml',
        # 'views/mrp_views.xml'
    ],
    'installable': 'True',
}